# Volantes Cursors Fork

### Manual Install

1. Install dependencies:

    - git
    - make
    - inkscape
    - xcursorgen

2. Run the following commands as normal user:

    ```
    git clone https://gitlab.com/sonakinci41/volantes-cursors-milis.git
    cd volantes-cursors-milis
    make build
    sudo make install
    ```

3. Choose a theme in the Settings or in the Tweaks tool.



